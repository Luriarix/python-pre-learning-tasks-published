def vowel_swapper(string):
    # ==============
    # Your code here
    # ==============
    vowels = {
        "a":0,
        "e":0,
        "i":0,
        "u":0,
        "o":0
        }
    swappers = {
        "a":"4",
        "A":"4",
        "e":"3",
        "E":"3",
        "i":"!",
        "I":"!",
        "u":"|_|",
        "U":"|_|",
        "o":"ooo",
        "O":"000"
        }
    n = 0
    temp = ""
    while n < string.__len__():
        if string[n].lower() in vowels:
            vowels[string[n].lower()] += 1
            if vowels[string[n].lower()] == 2:
                temp += swappers[string[n]]
            else:
                temp += string[n]
        else:
            temp += string[n]
        n += 1
    return temp

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
