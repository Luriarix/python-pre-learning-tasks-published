def vowel_swapper(string):
    # ==============
    # Your code here
    # ==============
    vowels = ("a","e","i","u","o","O")
    swappers = ("4","3","!","|_|","ooo","000")
    n = vowels.__len__() - 1
    while n != -1 :
        string = string.replace(vowels[n],swappers[n])
        string = string.replace(vowels[n].upper(),swappers[n])
        n -= 1
    return string


print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console